<?php
$array = [3,7,4,7,99,23,1,45,77,78,17,69,420,113,131,334,344,433,532,11,12,13,18,32,43,54,65,76,87,98,90];

function mergeSort(array $toSort)
{
    $len = count($toSort);
    if($len==1)
    {
        return $toSort;
    }
    $mid = (int) $len/2;

    $l1 = mergeSort(array_slice($toSort,$mid));
    $l2 = mergeSort(array_slice($toSort,0,$mid));

    return merge($l1,$l2);
}

function merge($arr1,$arr2)
{
    $resoultArray = [];
    while (count($arr1)>0 && count($arr2)>0)
    {
        if($arr2[0]<$arr1[0])
        {
            $resoultArray[]=$arr2[0];
            array_shift($arr2);
        }else
        {
            $resoultArray[]=$arr1[0];
            array_shift($arr1);
        }
    }

    $resoultArray = array_merge($resoultArray,$arr1);
    $resoultArray = array_merge($resoultArray,$arr2);

    return $resoultArray;
}



echo implode(",",$array);
$array = mergeSort($array);
echo "<br>";

echo implode(",",$array);