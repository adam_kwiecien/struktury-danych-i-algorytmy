<?php
$array = [3,7,4,7,99,23,1,45,77,78,17];

function bubbleSort(array &$toSort,bool $desc = false)
{
    $len = count($toSort);
    $bound = $len-1;

    for ($i=0;$i<$len;$i++)
    {
        $swap = false;
        $newBound=0;
        for ($j=0;$j<$bound;$j++)
        {
            if($desc && $toSort[$j]<$toSort[$j+1])//malejąco
            {
                $pom = $toSort[$j];
                $toSort[$j] = $toSort[$j+1];
                $toSort[$j+1] = $pom;
                $swap = true;
                $newBound = $j;
            }elseif(!$desc && $toSort[$j]>$toSort[$j+1])//rosnąco
            {
                $pom = $toSort[$j];
                $toSort[$j] = $toSort[$j+1];
                $toSort[$j+1] = $pom;
                $swap = true;
                $newBound = $j;
            }
        }

        $bound = $newBound;
        if(!$swap)
            return;
    }
}

print_r($array);
bubbleSort($array,);
echo "<br>";
print_r($array);
