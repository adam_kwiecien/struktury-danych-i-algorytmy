<?php

class TreeNode{

    private $data;
    private array $children;

    public function __construct()
    {
        $this->data = null;
        $this->children = [];
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param $child
     */
    public function addChild(TreeNode $child): void
    {
        $this->children[] = $child;
    }

}


class Tree{
    private ?TreeNode $root;

    public function __construct()
    {
        $this->root  = null;
    }

    /**
     * @return null
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param null $root
     */
    public function setRoot($root): void
    {
        $this->root = $root;
    }

    public function traverse(TreeNode $node=null,int $level = 0)
    {
        if($node === null) {
            $node = $this->root;
        }

        echo str_repeat('-',$level);
        echo $node->getData()."<br>";
        foreach ($node->getChildren() as $nodesChild)
        {
            $this->traverse($nodesChild,$level+1);
        }

    }
}

$tree = new Tree();

$ceo = new TreeNode();
$ceo->setData('prezes');

$cto = new TreeNode();
$cto->setData('dyrektor ds. technicznych');

$cfo = new TreeNode();
$cfo->setData('dyrektor ds. finansowych');
$cfoAssistant = new TreeNode();
$cfoAssistant->setData('asystent dyrektora ds. finansowych');

$cmo = new TreeNode();
$cmo->setData('dyrektor ds. marketingowych');

$coo = new TreeNode();
$coo->setData('dyrektor ds. operacyjnych');

$tree->setRoot($ceo);
$ceo->addChild($cto);
$ceo->addChild($cfo);
$ceo->addChild($cmo);
$ceo->addChild($coo);

$cfo->addChild($cfoAssistant);

$tree->traverse();